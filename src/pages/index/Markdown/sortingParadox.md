# The paradox

When I was in high school, I used to play Minecraft with my friends frequently. 

For those unfamiliar with minecraft, part of the gameplay is about players, going around digging and killing for a variety of items. Due to the player's bag limited capacity, players have to go "home" once in awhile to store their loots into chests. A common practice was to create a chest for each item, labeled, and store the items into their respective chest after each expedition. Everyone thought the same - if we organize our loot, it would be more effecient to search for them next time.

As the variety of items increased, the number of chest increased. To keep the effeciency, we group chest of the same categories in the same floor of the building, and chest of the same sub-category in the same room within the floor. Eventually, sorting does not add up as (a) it takes alot of time to traverse the floor-room-chest graph just to store each item after each expedition, (b) the same traversal is required for finding item, and (c) traversal takes alot of time, as it limited by the character's movement speed, accuracy and vision to look the correct label. If we were to not sort, and simple dump all our items in the next empty chest, we would (a) spend no time after each expedition, (b) take as much time as number of chest (each chest can store 54 different items) and (c) we will not need to move around significantly (close to 0). 

For sorting to be more effecient, the time taken to traverse the floor-room-chest graph has to be at least doubly more effecient than iteratively searching all existing chest that is not sorted, which will probably never happen. It confused me. What intended to be making process more effecient - sort, organizing, structuring - made it less effecient.

# Typing system

My programming language started with statically-typed background. My first language was VB, and my school taught me C++, but the first language that I would have come truly consider my first language was Java. For a few years, I wrote in Java (on hindside, this makes me sad) extensively, writing small programs, swing applications and eventually Minecraft mods. 

My first encounter with a dynamically typed was python. When I first wrote in python, my intuition was screaming to me that this is dangerously. But counter-intuitively, I was met with little problems - and more importantly, the time taken to develop scripts or small applications reduced by literal magnitudes. Understanding and abusing the duck-typing, obtaining polymorphism for free made me develop at unbelievable speed. 

This was about the time I started buying into the idea of dynamic typing - develop much faster, with more freedom! It was about this time I picked up both Javascript and Ruby, which I grew to love - especially Ruby (really, this could take a whole essay itself). It just made so much sense - I felt that I was in control, I felt light and I could go fast, breaking free of the type-system Java unfairly placed upon me.

Akin to sorting items into chests, structuring a type system - what intended to make the language more effecient - made me develop slower. What was the biggest concern for me was, at that time, research stated that development is about 3 times faster in a dynamically typed language than a statically typed language - and there are many large companies willing to incur the speed loss for "stability".  

# The Fallacy

Shortly after I started my first job as a software engineer, I found the fallacy with the paradox. The root of this fallacy, on hindsight, was from scoped thinking - our brains tend to contextualize memory from experiences, not from facts. Your brain would only vividly remember the times you missed the bus, but not all the time you managed to board it. Your memory would then only contain of incidents where you missed the bus, leading to your belief that you have bad timing.

Similarly, all my examples I drew my belief of the paradox of sorting was (a) solo or small-group projects, (b) small-function applications and (c) experience that spans over a short time period. Both the Minecraft game, my small applications in Python was both (a) involved only me and/or 1-2 friends (b) only catered to me and/or 1-2 friends (c) and lasting only over 2 - 3 month, without ever coming back to it. 


