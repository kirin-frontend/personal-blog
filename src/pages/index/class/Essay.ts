interface Day {
	day: number
	month: number
	year: number
}

interface EssayLiteral {
	title: string;
	markdown: string;
	date: Day;
	tag: string;
}

const emptyLiteral: EssayLiteral = {
	title: "",
	markdown: "",
	date: {day: 0, month: 0, year: 0},
	tag: "",
};

class Essay {
	private readonly title: string;
	private readonly markdown: string;
	private readonly date: Day;
	private readonly tag: string;
	public static EMPTY: Essay = new Essay(emptyLiteral);
	
	private monthToText(month: number): string {
		switch (month) {
			case 1:
				return "January";
			case 2:
				return "February";
			case 3:
				return "March";
			case 4:
				return "April";
			case 5:
				return "May";
			case 6:
				return "June";
			case 7:
				return "July";
			case 8:
				return "August";
			case 9:
				return "September";
			case 10:
				return "October";
			case 11:
				return "November";
			case 12:
				return "December";
		}
		return "Unknown"
	}
	
	constructor(literal: EssayLiteral) {
		this.title = literal.title;
		this.markdown = literal.markdown;
		this.date = literal.date;
		this.tag = literal.tag;
	}
	
	get Title(): string {
		return this.title;
	}
	
	get Markdown(): string {
		return this.markdown;
	}
	
	get Date(): string {
		if (this == Essay.EMPTY) {
			return "";
		}
		return this.monthToText(this.date.month).Take(3).toUpperCase() + " " + this.date.year;
	}
	
	get DateFull(): string {
		if (this == Essay.EMPTY) {
			return "";
		}
		return this.date.day + " " + this.monthToText(this.date.month) + " 20" + this.date.year;
	}
	
	get Tag(): string {
		return this.tag;
	}
}

export {Essay, EssayLiteral}
