import Vue from 'vue';
import App from './App.vue';
import './index.scss';
import 'highlight.js/styles/atom-one-light.css';
import {router} from "./router";

Vue.config.productionTip = false;

new Vue({
	router,
	render: h => h(App)
}).$mount('#app');
