import Vue from 'vue'
import Router, {RouteConfig} from 'vue-router'
import NotFound from "./views/NotFound.vue";
import Essays from "./views/Essays.vue";
import {essays} from "./init";
import {Essay} from "./class/Essay";

Vue.use(Router);

const get = (markdown: string) => require(`./Markdown/${markdown}.md`)["default"];


const routes: RouteConfig[] = [
	{path: '/', name: 'home', component: Essays},
	{path: '*', name: '404', component: NotFound},
];

const map: { [s: string]: Essay } = {};

essays.Each(e => {
	routes.push({path: "/" + e.Markdown, name: e.Markdown, component: get(e.Markdown)});
	map[e.Markdown] = e;
});

const router: Router = new Router({
	mode: "history",
	base: process.env.BASE_URL,
	routes,
	scrollBehavior(to, from, savedPosition) {
		if (savedPosition) {
			return savedPosition
		} else if (to.hash) {
			return {selector: to.hash};
		} else {
			return {x: 0, y: 0}
			
		}
	}
});

export {
	router,
	map as essayMap,
};
